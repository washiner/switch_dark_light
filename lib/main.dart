import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  ThemeData _lightTheme = ThemeData(
    accentColor: Colors.black87,
    brightness: Brightness.light,
    primaryColor: Colors.blue,
  );

  ThemeData _darkTheme = ThemeData(
    accentColor: Colors.red,
    brightness: Brightness.dark,
    primaryColor: Colors.amber,
  );

  bool _light = true;
  String luz = "Desligue a luz";
  String semLuz = "Acenta a luz";

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: _light ? _lightTheme : _darkTheme,
      home: Scaffold(
        appBar: AppBar(
        title: Text(_light ? luz : semLuz) ,
        ),
        body: Column(
          children: [
            Image(image: AssetImage("lib/assets/foto4.png")),
            Center(
              child: Switch(
                autofocus: true,
                value: _light,
                onChanged: (state){
                  setState(() {
                    _light = state;
                  });
                },
              ),
            ),
            Text("interruptor", style: TextStyle(fontSize: 22),)
          ],
        ),
      )
    );
  }
}

